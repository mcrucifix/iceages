iceages
=======

Companion package to paper 'Why ice ages could be unpredictable', to be submitted to Earth and Planetary Science Reviews

!!! When using this package for production, always cite the authors of the original models !!! 
!!! All details available in the manpages                                                  !!! 
